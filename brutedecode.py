#Usage: python brutedecode.py <message> <book>
import sys, re

def main(args):
    with open(args[1], 'r') as thefile:
        thetext = thefile.read()

    codes = re.findall('\d+', thetext)

    #Split into individual pads
    padlength = 51 #10 rows of 5 + 1 initial number
    pads = []
    for i in range(0, len(codes), 102):
        pad1 = []
        pad2 = []
        for j in range(0, 100, 10):
            pad1.extend(codes[i+j+2:i+j+7])
            pad2.extend(codes[i+j+7:i+j+12])
        pads.append(pad1)
        pads.append(pad2)

    #remove first thing from each pad
    for pi in range(0, len(pads)):
        pads[pi] = pads[pi][1:]

    #decode
    with open(args[0], 'r') as thefile:
        themessage = thefile.read()
    msg = re.findall('\d+', themessage)
    msg = msg[1:] #remove first thing

    results = []
    for p in pads:
        res = []
        for i in range(0, len(msg)):
            for h in range(0,5):
                res.append(str(int(msg[i][h]) + int(p[i][h]))[-1]) #add and take only unit
        results.append("".join(res))

    #numbers to letters
    #populate dictionary
    codekey = { 0: "code",
                1: "A", 2: "E", 3: "I", 4: "N", 5: "O", 6: "T",
                90: "fig", 98: "req", 99: "spc"}

    letters = "BCDFGHJKLMPQRSUVWXYZ"
    
    for n in range(0, len(letters)):
        codekey[70+n] = letters[n]

    characters = ".:' +-="

    for c in range(0, len(characters)):
        codekey[91+c] = characters[c]

    #decryption time
    n = 0
    while (n < len(results)):
        print n
        m = 0
        while (m < len(results[n])):
            cr = int(results[n][m])
            if cr > 6 and m < len(results[n])-1:
                cr = (10*cr)+int(results[n][m+1])
                m += 1
            if cr == 0:
                m += 3

            if cr in codekey:
                print(codekey[cr]),
                if cr == 0 and m < len(results[n]):
                    print("{0}{1}{2}".format(results[n][m-2],results[n][m-1],results[n][m]))

            m += 1
        print("\n")
        n += 1

if __name__ == "__main__":
    main(sys.argv[1:])
