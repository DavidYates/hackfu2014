
=======================
HACKFU CHALLENGE - 2014
=======================

Welcome to the strike team agent. By solving the first container challenge you have unlocked access to the rest of this year's puzzles. We will track and try to apprehend Ilichy, a dangerous enemy agent bent on world domination and chaos.

The information your unit found after decrypting the message provided you with new clues about Ilichy's whereabouts and activities. There are more clues to find, each located in the folders here. You can do them in any order you like.

Each challenge has a specific answer that you need to send back to HQ, so that we can keep track of your progress. In most cases, instructons will be given to you only after solving the clue. Similar to this challenge that you just solved! (Well done, we knew you could do it!)

Please let us know that you have opened the cookie jar, send us an email:

Subject: Hackfu Challenge 2014 - Container - Uber Russian Doll
Email address: redbear2014@mwrinfosecurity.com
Body: Tell us about your solution, include some code if you want :)

Also in each challenge you will find a fragment of a larger passphrase. Combining all the fragments (without the quotes), will enable you to decrypt the Epilogue to see the conclusion of Ilichy Prime. The Epilogue is encrypted using aes-256-cbc. 
